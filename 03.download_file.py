#Import thread beserta library2 yang dibutuhkan
import os
import requests
import threading
import urllib.request, urllib.error, urllib.parse
import time

#link URL yang akan diunduh
url = "https://apod.nasa.gov/apod/image/1901/LOmbradellaTerraFinazzi.jpg"


def buildRange(value, numsplits):
    lst = []
    for i in range(numsplits):
        if i == 0:

            # Menambah elemen untuk array lst
            lst.append('%s-%s' % (i, int(round(1 + i * value/(numsplits*1.0) + value/(numsplits*1.0)-1, 0))))
        else:
            lst.append('%s-%s' % (int(round(1 + i * value/(numsplits*1.0),0)), int(round(1 + i * value/(numsplits*1.0) + value/(numsplits*1.0)-1, 0))))
    return lst

class SplitBufferThreads(threading.Thread):
    """ Splits the buffer to ny number of threads
        thereby, concurrently downloading through
        ny number of threads.
    """
    def __init__(self, url, byteRange):

        # Memisah antara buffer setiap thread dengan byte range yang telah ditentukan
        super(SplitBufferThreads, self).__init__()
        self.__url = url
        self.__byteRange = byteRange
        self.req = None
    def run(self):
        self.req = urllib.request.Request(self.__url,  headers={'Range': 'bytes=%s' % self.__byteRange})

    def getFileData(self):
        return urllib.request.urlopen(self.req).read()


def main(url=None, splitBy=3):
    start_time = time.time()
    if not url:
        print("Please Enter some url to begin download.")
        return

    #Membagi nama dari file bedasarkna (/) nama file dari path URL di bagian paling akhir
    fileName = url.split('/')[-1]

    #Pengukuran besar file yang akan diunduh
    sizeInBytes = requests.head(url, headers={'Accept-Encoding': 'identity'}).headers.get('content-length', None)

    #Menampilkan output pengukuran besar file yang telah dilakukan diatas
    print("%s bytes to download." % sizeInBytes)

    #Jika isi dalam byte kosong maka program selesai
    if not sizeInBytes:
        print("Size cannot be determined.")
        return

    #Pendeklarasian List
    dataLst = []

    #Looping sejumlah splitby
    for idx in range(splitBy):

        #Membagikan byte data sesuai splitby
        byteRange = buildRange(int(sizeInBytes), splitBy)[idx]

        #Menetapkan thread dengan job yang sesuai dari pembagian yang telah dilakukan oleh byterange
        bufTh = SplitBufferThreads(url, byteRange)

        #Memulai pengauploadan
        bufTh.start()

        #Maintread menunggu pekerjaan dari childthread selesai dilaksakan
        bufTh.join()

        #Menambahkan data yang sudah diunggah kedalam list yang telah dibuat
        dataLst.append(bufTh.getFileData())

    #Menggabungkan isi dari datalst
    content = b''.join(dataLst)

    #Jika datalst berhasil maka
    if dataLst:

        #Jika didalam folder terdapat nama file yang sama
        if os.path.exists(fileName):

            #Menghapus file dengan nama file yang sama
            os.remove(fileName)

        #Output berapa lama waktu yang perlukan untuk menyelesaikan pekerjaan
        print("--- %s seconds ---" % str(time.time() - start_time))
        with open(fileName, 'wb') as fh:

            #Menulis konten dari file
            fh.write(content)

        #Output untuk menyatakan pekerjaan selesai dilakukan
        print("Finished Writing file %s" % fileName)

if __name__ == '__main__':

    # eksekusi program utama
    main(url)